const webpack = require("webpack");
const path = require("path");
const HTMLWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: "production",
  entry: `${__dirname}/src/index.js`,
  output: { path: `${__dirname}/dist`, filename: "index.js" },
  module: {
    rules: [
      { test: /\.(js|jsx)$/, exclude: /node_modules/, use: ["babel-loader"] },
      { test: /\.(jpeg|png|jpg)$/, use: "file-loader" },
      { test: /\.svg$/, use: "url-loader" },
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, "./src"),
        use: ["style-loader", "css-loader", "sass-loader"]
      }
    ]
  },
  resolve: { extensions: ["*", ".js", ".jsx"] },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HTMLWebpackPlugin({
      template: "./public/index.html"
    })
  ],
  devServer: { contentBase: "./dist", hot: true }
};
