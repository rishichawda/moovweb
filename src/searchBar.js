import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCityFromPincode } from './actions';
import cityList from './cityList';

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: ''
        };
    }
    
    onSubmit = (e) => {
        e.preventDefault();
        const { fetchCity, cities } = this.props;
        const { input } = this.state;
        if(!(cities.filter(city => city.pincode === input)).length) {
            fetchCity(input);
        }
    }

    onInputChange = ({ target }) => {
        this.setState({
            input: target.value
        })
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <input type="text" value={this.state.input} pattern="[0-9]{5}" onChange={this.onInputChange}/>
                <button type="submit">search</button>
            </form>
        );
    }
}

const mapDispatch = dispatch => {
    return {
        fetchCity : bindActionCreators(getCityFromPincode, dispatch)
    };
}

const getcities = ({ cities }) => ({ cities })

export default connect(getcities, mapDispatch)(SearchBar);