import { combineReducers } from 'redux';
import citiesReducer from './citylist_reducer';

const rootReducer = combineReducers({
    cities: citiesReducer
})

export default rootReducer;