import { GET_CITY } from "../shared/actionTypes";

export default function ( state = [], action ) { 
    switch (action.type) {
        case GET_CITY: 
            return [ action.payload, ...state ]
        default: {}
    }
    return state;
}