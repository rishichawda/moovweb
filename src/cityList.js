import React from 'react';
import { connect } from 'react-redux';

const CityList = ({ cities }) => {
    return (
        <ul>
            {
                cities.map( city => <li key={city.pincode}>{city.name}</li> )
            }
        </ul>
    )
}

const getCityList = ({ cities }) => ({ cities });

export default connect(getCityList, null)(CityList);