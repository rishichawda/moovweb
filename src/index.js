import React from "react";
import ReactDOM from "react-dom";
import store from './store';
import CityList from "./cityList";
import { Provider } from 'react-redux';
import SearchBar from './searchBar';

const App = () => (
  <div className="app">
    <SearchBar />
    <CityList />
  </div>
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById("root"));
