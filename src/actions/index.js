import { GET_CITY } from "../shared/actionTypes";
import axios from 'axios';

export function getCityFromPincode(pincode) {
    const url = `https://api.zippopotam.us/us/${pincode}`;
    const response = axios.get(url);
    return response.then(res => {
        return {
            type: GET_CITY,
            payload: { pincode, name: res.data.places[0]["place name"] },
        }
    });
}